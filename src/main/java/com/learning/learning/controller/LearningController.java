package com.learning.learning.controller;

import com.learning.learning.dto.*;
import com.learning.learning.model.Panen;
import com.learning.learning.model.User;
import com.learning.learning.service.LearningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class LearningController {

    @Autowired
    LearningService learningService;

    @GetMapping(value = "/getpersonname")
    public String getPersonName(@RequestParam(value = "gender", defaultValue = "gender") String gender) {
        return learningService.getName(gender);
    }

    @GetMapping(value = "/getpersonnamev2")
    public Person getPersonNameV2(@RequestParam(value = "gender", defaultValue = "gender") String gender) {
        return learningService.getNameJson(gender);
    }

    @PostMapping(
            value = "/getpersonnamev3",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public String getPersonNameV3(@RequestBody(required = true) Person person) {
        return learningService.getNameV3(person.getName());
    }

    @PostMapping(
            value = "/register",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public String register(@RequestBody(required = true) UserRequest userRequest) {
        return learningService.register(userRequest);
    }

    @PostMapping(
            value = "/login",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public User login(@RequestBody(required = true) LoginRequest loginRequest) {
        return learningService.login(loginRequest);
    }

    @GetMapping(value = "/person")
    public Person getDataPerson(
            @RequestParam(value = "name", defaultValue = "No Name") String name,
            @RequestParam(value = "age", defaultValue = "12") Integer age
            ) {
        return learningService.getDataPerson(name, age);
    }

    @PostMapping(
            value = "/getperson",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public PersonDtoResponse getPerson(@RequestBody(required = true) PersonRequest request) {
        return learningService.getPerson(request);
    }

    @GetMapping(value = "/panen")
    public List<PanenRsDto> submitData(){
        return learningService.getPanen();
    }

    @GetMapping(value = "/panen/summary")
    public PanenRsDto getSummaryDataByAvgKg(){
        return learningService.getSummary();
    }
}
