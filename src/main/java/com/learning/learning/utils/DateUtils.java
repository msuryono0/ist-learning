package com.learning.learning.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    public String dateTimeFormat(Date date, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }
}
