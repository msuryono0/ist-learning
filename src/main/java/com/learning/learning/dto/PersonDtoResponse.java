package com.learning.learning.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class PersonDtoResponse {
    private String name;
    private Object data;
}
