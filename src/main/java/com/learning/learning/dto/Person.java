package com.learning.learning.dto;

public class Person {

    private String name;

    private Integer Age;

    public Integer getAge() {
        return Age;
    }

    public void setAge(Integer age) {
        Age = age;
    }

    public Person() {
    }

    public Person(String name, Integer age) {
        setName(name);
        setAge(age);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
