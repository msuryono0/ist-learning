package com.learning.learning.repository;

import com.learning.learning.model.Panen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PanenRepository extends JpaRepository<Panen, Long> {



}
