package com.learning.learning.service;

import com.learning.learning.dto.*;
import com.learning.learning.model.Panen;
import com.learning.learning.model.User;
import com.learning.learning.repository.PanenRepository;
import com.learning.learning.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class LearningService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    PanenRepository panenRepository;


    public String getName(String gender) {

        Person person = new Person();
        if ("man".equals(gender)) {
            person.setName("John");
        } else {
            person.setName("Jane");
        }

        return person.getName();
    }

    public Person getNameJson(String gender) {
        Person person = new Person();
        if ("man".equals(gender)) {
            person.setName("John");
        } else {
            person.setName("Jane");
        }

        return person;
    }

    public String getNameV3(String name) {
        String fullName = "not identified";

        if ("John".equals(name)) {
            fullName = "John Doe";
        } else {
            fullName = "Jane Doe";
        }

        return fullName;
    }

    public PersonDtoResponse getPerson(PersonRequest personRequest) {
        LocalDate today = LocalDate.now();
        Integer age = today.getYear() - Integer.parseInt(personRequest.getYearOfBirth());

        Map<String, Integer> map = new HashMap<>();
        map.put("age", age);
        return PersonDtoResponse.builder().name(personRequest.getName()).data(map).build();
    }

    public String register(UserRequest userRequest) {
        User existUser = userRepository.findByUserName(userRequest.getUsername());
        if (existUser != null) {
            return "Register failed, username already exist!";
        }

        User user = new User();
        user.setUserName(userRequest.getUsername());
        user.setPassWord(userRequest.getPassword());
        userRepository.save(user);

        return "Register success";
    }

    public User login(LoginRequest loginRequest) {
        User existUser = userRepository.findByUserNameAndPassWord(loginRequest.getUsername(), loginRequest.getPassword());
        if (existUser == null) {
            return null;
        }
        return existUser;
    }

    public Person getDataPerson(String name, Integer age) {
        return new Person(name, age);
    }

    public User getDataUser(String username) {
        return userRepository.findByUserName(username);
    }

    public List<PanenRsDto> getPanen(){
        List<Panen> panenList = panenRepository.findAll();
        List<PanenRsDto> panenRsDtoList = new ArrayList<>();

        panenList.forEach(item -> {
            panenRsDtoList.add(PanenRsDto.builder()
                    .penjual(item.getPenjual())
                    .totalEkor(item.getTotalEkor())
                    .berat(item.getBerat())
                    .umur(item.getUmur())
                    .grade(item.getGrade())
                    .avgKg(item.getBerat() / item.getTotalEkor())
                    .build());
        });
        return panenRsDtoList;
    }

    public PanenRsDto getSummary(){
        List<Panen> panenList = panenRepository.findAll();
        List<PanenRsDto> panenRsDtoList = new ArrayList<>();

        panenList.forEach(item -> {
            panenRsDtoList.add(PanenRsDto.builder()
                    .avgKg(item.getBerat() / item.getTotalEkor())
                    .build());
        });

        Double summary = panenRsDtoList.stream()
                .mapToDouble(PanenRsDto::getAvgKg)
                .sum();

        return PanenRsDto.builder().summaryAvg(summary).build();
    }
}
