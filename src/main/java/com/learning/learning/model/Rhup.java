package com.learning.learning.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "rhup")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Rhup {

    @Id
    private Long id;
    private Character grade;
    private Float minimum;
    private Float maximum;
    private Integer ekor;
    private Float tonaseKg;
    private Float avgUmur;
}
