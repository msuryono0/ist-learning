package com.learning.learning.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "panen")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Panen {

    @Id
    private Long id;
    private String penjual;
    private Integer totalEkor;
    private Float berat;
    private Integer umur;
    private Character grade;
}
