package com.learning.learning;

import com.learning.learning.dto.Person;
import com.learning.learning.dto.UserRequest;
import com.learning.learning.model.User;
import com.learning.learning.repository.UserRepository;
import com.learning.learning.service.LearningService;
import com.learning.learning.utils.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class LearningServiceTest {

    @Autowired
    private LearningService learningService;

    @MockBean
    @Autowired
    private UserRepository userRepository;

    @Test
    public void getUser_shouldNotNull() {
        Mockito.when(userRepository.findByUserName("muhammad")).thenReturn(new User());
        Assertions.assertNotNull(learningService.getDataUser("muhammad"));
    }

    @Test
    public void getUser_shouldNull() {
        Mockito.when(userRepository.findByUserName("muhamsmad")).thenReturn(null);
        Assertions.assertNull(learningService.getDataUser("muhamsmad"));
    }

    @Test
    public void getPerson_shouldNotNull() {
        Person person = learningService.getDataPerson("muhamsmad", 12);
        Assertions.assertNotNull(person);
    }

    @Test
    public void register_shouldEquals() {
        UserRequest userRequest = new UserRequest(StringUtils.getSaltString(), "123");
        String resp = learningService.register(userRequest);
        Assertions.assertEquals("Register success", resp);
    }

    @Test
    public void getName_shouldEquals() {
        String name = learningService.getName("man");
        Assertions.assertEquals("John", name);
    }

    @Test
    public void getName_shouldNotEquals() {
        String name = learningService.getName("not_man");
        Assertions.assertNotEquals("John", name);
    }

    @Test
    public void getName_shouldNotNull() {
        Person person = learningService.getNameJson("not_man");
        Assertions.assertNotNull(person);
    }

    @Test
    public void getUserName_shouldNotNull() {
        User user = userRepository.findByUserName("muhammad");
        Assertions.assertNull(user);
    }

    @Test
    public void getFullName_shouldEquals() {
        String name = learningService.getNameV3("muhammad");
        Assertions.assertEquals("Jane Doe", name);
    }
}
