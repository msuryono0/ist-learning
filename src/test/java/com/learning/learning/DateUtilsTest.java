package com.learning.learning;

import com.learning.learning.utils.DateUtils;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class DateUtilsTest {
    Date date;
    String resultFormat;

    @Before("")
    public void init() {
        date = Date.from(LocalDate.of(2019, 01, 22).atStartOfDay(ZoneId.systemDefault()).toInstant());
        resultFormat = new DateUtils().dateTimeFormat(date, "yyyyMMMdd");
    }

    @Test
    public void dateTimeFormat_shouldSuccess(){
        date = Date.from(LocalDate.of(2019, 01, 22).atStartOfDay(ZoneId.systemDefault()).toInstant());
        resultFormat = new DateUtils().dateTimeFormat(date, "yyyyMMMdd");
        Assertions.assertEquals("2019Jan22", resultFormat);
    }

    @Test
    public void dateTimeFormat_shouldNotSuccess(){
        date = Date.from(LocalDate.of(2019, 01, 22).atStartOfDay(ZoneId.systemDefault()).toInstant());
        resultFormat = new DateUtils().dateTimeFormat(date, "yyyyMMMdd");
        Assertions.assertNotEquals("2018Jan22", resultFormat);
    }
}
